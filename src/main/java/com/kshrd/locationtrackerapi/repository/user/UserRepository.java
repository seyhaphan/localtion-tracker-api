package com.kshrd.locationtrackerapi.repository.user;

import com.kshrd.locationtrackerapi.payload.user.UserRequest;
import com.kshrd.locationtrackerapi.payload.user.UserResponse;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;


@Repository
public interface UserRepository {

    //TODO: Insert User
    @InsertProvider(type = UserProvider.class,method = "insertUserSql")
    @Options(useGeneratedKeys = true, keyProperty = "userId", keyColumn = "user_id")
    boolean insertUser(UserRequest userRequest);

    //TODO: Find One User By Id
    @Select("select * from \"user\" where user_id = #{id}")
    UserResponse findUserById(Integer id);
}
