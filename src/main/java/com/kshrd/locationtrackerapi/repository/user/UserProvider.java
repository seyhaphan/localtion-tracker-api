package com.kshrd.locationtrackerapi.repository.user;

import com.kshrd.locationtrackerapi.payload.user.UserRequest;
import org.apache.ibatis.jdbc.SQL;

public class UserProvider {

    public String insertUserSql(UserRequest userRequest){
        return new SQL(){{
            INSERT_INTO("\"user\"");
            VALUES("username","#{username}");
            VALUES("email","#{email}");
            VALUES("password","#{password}");
            VALUES("photo","#{photo}");
            VALUES("user_type_id","#{userTypeId}");
            VALUES("location_id","#{locationId}");
            VALUES("created_date","#{createdDate}");
            VALUES("modified_date","#{modifiedDate}");
            VALUES("status","#{status}");
        }}.toString();
    }
}
