package com.kshrd.locationtrackerapi.exception;

import com.kshrd.locationtrackerapi.message.ErrorResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Collections;

@ControllerAdvice
@RestController
public class CustomizedResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(SQLException.class)
    public final ResponseEntity<ErrorResponse> handleSqlException(Exception ex, WebRequest request) {
        ErrorResponse errorResponse = new ErrorResponse();

        errorResponse.setMessage(ex.getCause().getMessage().split("Detail:")[1]);
        errorResponse.setError(Collections.singletonList(ex.getCause().getMessage().split("Detail:")[0]));
        errorResponse.setStatus(HttpStatus.CONFLICT);
        errorResponse.setTime(new Timestamp(System.currentTimeMillis()));

        return ResponseEntity.ok(errorResponse);
    }

}
