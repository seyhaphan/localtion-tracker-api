package com.kshrd.locationtrackerapi.service.user;

import com.kshrd.locationtrackerapi.payload.user.UserRequest;
import com.kshrd.locationtrackerapi.payload.user.UserResponse;
import org.springframework.stereotype.Service;

@Service
public interface UserService {

    //TODO: Insert user ============================
    UserRequest insertUser(UserRequest userRequest);

    //TODO: Find One User By id
    UserResponse findUserById(Integer id);
}
