package com.kshrd.locationtrackerapi.service.user;

import com.kshrd.locationtrackerapi.payload.user.UserRequest;
import com.kshrd.locationtrackerapi.payload.user.UserResponse;
import com.kshrd.locationtrackerapi.repository.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImp implements UserService {
    private UserRepository userRepository;

    @Autowired
    public UserServiceImp(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserRequest insertUser(UserRequest userRequest){
        boolean isInserted = userRepository.insertUser(userRequest);
        return isInserted ? userRequest : null;
    }

    @Override
    public UserResponse findUserById(Integer id) {
        return  userRepository.findUserById(id);
    }
}
