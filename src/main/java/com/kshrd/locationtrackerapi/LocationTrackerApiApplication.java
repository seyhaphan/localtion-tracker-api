package com.kshrd.locationtrackerapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class LocationTrackerApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(LocationTrackerApiApplication.class, args);
    }

}
