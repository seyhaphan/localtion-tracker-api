package com.kshrd.locationtrackerapi.enums;

public enum UserStatus {
    TRUE('1'),
    FALSE('0');

    private final char value;

    UserStatus(char value) {
        this.value = value;
    }
    public char value() {
        return this.value;
    }
}
