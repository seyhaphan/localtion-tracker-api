package com.kshrd.locationtrackerapi.controller;

import com.kshrd.locationtrackerapi.enums.UserStatus;
import com.kshrd.locationtrackerapi.payload.user.UserRequest;
import com.kshrd.locationtrackerapi.payload.user.UserResponse;
import com.kshrd.locationtrackerapi.message.BaseApiResponse;
import com.kshrd.locationtrackerapi.message.ErrorResponse;
import com.kshrd.locationtrackerapi.message.MessageProperties;
import com.kshrd.locationtrackerapi.utils.ApiUtils;
import com.kshrd.locationtrackerapi.service.user.UserServiceImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class UserController {

    private UserServiceImp userServiceImp;
    private ApiUtils apiUtils;
    private MessageProperties messageProperties;

    @Autowired
    public void setUserServiceImp(UserServiceImp userServiceImp) {
        this.userServiceImp = userServiceImp;
    }

    @Autowired
    public void setApiUtils(ApiUtils apiUtils) {
        this.apiUtils = apiUtils;
    }

    @Autowired
    public void setMessageProperties(MessageProperties messageProperties) {
        this.messageProperties = messageProperties;
    }

    //TODO: Handle Exception Error Wrong Field Name ===================================================
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ErrorResponse> handleValidationExceptions(MethodArgumentNotValidException ex){

        ErrorResponse response = new ErrorResponse();
        List<Object> errors = new ArrayList<>();

        ex.getBindingResult().getAllErrors().forEach((error) -> {

            Map<String, String> objectError = new HashMap<>();
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            objectError.put("field", fieldName);
            objectError.put("message", errorMessage);
            errors.add(objectError);
        });

        response.setMessage(messageProperties.insertError("User"));
        response.setError(errors);
        response.setStatus(HttpStatus.BAD_REQUEST);
        response.setTime(new Timestamp(System.currentTimeMillis()));

        return ResponseEntity.ok(response);
    }

    //TODO: Insert User
    @PostMapping("/user")
    public ResponseEntity<BaseApiResponse<UserResponse>> insertUser(
            @Valid @RequestBody UserRequest userRequest){

        BaseApiResponse<UserResponse> response = new BaseApiResponse<>();

        userRequest.setStatus(UserStatus.TRUE.value());
        userRequest.setCreatedDate(apiUtils.getTimeStamp());
        userRequest.setModifiedDate(apiUtils.getTimeStamp());
//        try{
            UserRequest result = userServiceImp.insertUser(userRequest);

            UserResponse userResponse = apiUtils.getMapper().map(result,UserResponse.class);

            result.setUserId(result.getUserId());
            response.setData(userResponse);
            response.setMessage(messageProperties.inserted("User"));
            response.setStatus(HttpStatus.CREATED);

//        }catch (DuplicateKeyException e){
//            response.setMessage(messageProperties.insertError("User already exist. User"));
//            response.setStatus(HttpStatus.BAD_REQUEST);
//        }

        response.setTime(apiUtils.getTimeStamp());
        return ResponseEntity.ok(response);
    }

    @GetMapping("/user/{id}")
    public ResponseEntity<BaseApiResponse<UserResponse>> findUserById(@PathVariable Integer id){
        BaseApiResponse<UserResponse> response = new BaseApiResponse<>();

        UserResponse userResponse = userServiceImp.findUserById(id);

        if(userResponse == null){
            response.setMessage(messageProperties.hasNoRecord("User"));
            response.setStatus(HttpStatus.NO_CONTENT);
        }else{
            response.setMessage(messageProperties.selectedOne("User"));
            response.setData(userResponse);
            response.setStatus(HttpStatus.OK);
        }

        response.setTime(apiUtils.getTimeStamp());

        return ResponseEntity.ok(response);
    }

}


