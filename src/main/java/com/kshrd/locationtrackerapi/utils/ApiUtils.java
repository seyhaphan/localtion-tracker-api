package com.kshrd.locationtrackerapi.utils;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;

@Component
public class ApiUtils {
    public ModelMapper getMapper(){
        return new ModelMapper();
    }
    public Timestamp getTimeStamp(){
        return new Timestamp(System.currentTimeMillis());
    }

//    public BCryptPasswordEncoder passwordEncoder(){
//        return new BCryptPasswordEncoder();
//    }

}
