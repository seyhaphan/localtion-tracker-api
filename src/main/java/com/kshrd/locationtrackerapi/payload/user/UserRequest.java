package com.kshrd.locationtrackerapi.payload.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.sql.Timestamp;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class UserRequest {

    @JsonIgnore
    private Integer userId;

    @ApiModelProperty(value = "Username", position = 1)
    @NotBlank(message = "Username cannot be empty")
    private String username;

    @ApiModelProperty(value = "Email", position = 2)
    @NotBlank(message = "Email cannot be empty")
    private String email;

    @ApiModelProperty(value = "Password", position = 5)
    @NotBlank(message = "Password cannot be empty")
    private String password;

    @ApiModelProperty(value = "Photo", position = 4)
    private String photo;

    @ApiModelProperty(value = "User Type Id", position = 5)
    private Integer userTypeId;

    @ApiModelProperty(value = "User Location Id", position = 6)
    private int locationId;

    @JsonIgnore
    private Timestamp createdDate;

    @JsonIgnore
    private Timestamp modifiedDate;

    @JsonIgnore
    private char status;

}
