package com.kshrd.locationtrackerapi.payload.user;

import com.kshrd.locationtrackerapi.domain.UserLocation;
import com.kshrd.locationtrackerapi.domain.UserType;
import lombok.*;

import java.sql.Timestamp;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class UserResponse {

    private String userId;
    private String username;
    private String email;
    private String photo;
    private UserType userType;
    private UserLocation location;
    private Timestamp createdDate;
    private Timestamp modifiedDate;
    private char status;

}
