package com.kshrd.locationtrackerapi.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;


@AllArgsConstructor
@NoArgsConstructor
@Data
public class User {

    private String userId;
    private String username;
    private String email;
    private String password;
    private String photo;
    private int userTypeId;
    private int locationId;
    private Timestamp createdDate;
    private Timestamp modifiedDate;
    private char status;

}
