package com.kshrd.locationtrackerapi.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class UserLocation {

    private int locationId;
    private long latitude;
    private long longitude;
}
